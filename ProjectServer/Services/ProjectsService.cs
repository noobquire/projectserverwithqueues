﻿using System.Linq;
using ProjectServer.DAL;
using ProjectShared.DTO;
using ProjectServer.Interfaces;

namespace ProjectServer.Services
{
    public class ProjectsService : IProjectsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProjectsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ProjectStatsDTO GetProjectStats(int projectId)
        {

            return (from project in _unitOfWork.Projects.GetAll()
                    join task in _unitOfWork.Tasks.GetAll() on project.Id equals task.ProjectId into projectTasks
                    join user in _unitOfWork.Users.GetAll() on project.TeamId equals user.TeamId into projectTeam
                    select new ProjectStatsDTO
                    {
                        ProjectId = project.Id,

                        TaskWithLongestDescId = projectTasks.Any() ? projectTasks
                        .OrderByDescending(t => t.Description.Length)
                        .FirstOrDefault().Id : -1,

                        TaskWithShortestNameId = projectTasks.Any() ? projectTasks
                        .OrderBy(t => t.Name.Length)
                        .FirstOrDefault().Id : -1,

                        TotalUsersInProjectTeam =
                        (project.Description.Length > 25 || projectTasks.Count() < 3) ?
                        projectTeam.Count() : 0,
                    }).FirstOrDefault(ps => ps.ProjectId == projectId);

        }
    }
}
