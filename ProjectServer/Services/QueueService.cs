﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RabbitMQ.Client;
using System.Text;
using ProjectServer.Interfaces;

namespace ProjectServer.Services
{
    public class QueueService : IQueueService
    {
        private readonly IConfiguration _configuration;
        public QueueService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public bool PostValue(string message)
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri(_configuration.GetSection("RabbitMQ").Value),
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare("RequestExchange", ExchangeType.Direct);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(
                    exchange: "RequestExchange",
                    routingKey: "request",
                    basicProperties: null,
                    body: body);
                return true;
            }
        }
    }
}
