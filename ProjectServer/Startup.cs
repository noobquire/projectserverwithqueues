﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectServer.DAL;
using ProjectServer.Services;
using ProjectServer.Interfaces;
using ProjectServer.Hubs;

namespace ProjectServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSingleton(typeof(IUnitOfWork), typeof(UnitOfWork));

            services.AddScoped(typeof(IUsersService), typeof(UsersService));
            services.AddScoped(typeof(IProjectsService), typeof(ProjectsService));
            services.AddScoped(typeof(ITeamsService), typeof(TeamsService));
            services.AddScoped(typeof(IQueueService), typeof(QueueService));
            services.AddScoped(typeof(IFileService), typeof(FileService));

            services.AddSingleton(typeof(IMessageService), typeof(MessageService));

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app.UseHttpsRedirection();
            app.UseMvc();

            

            app.UseSignalR(routes =>
            {
                routes.MapHub<RequestHub>("/requesthub");
            });
        }
    }
}