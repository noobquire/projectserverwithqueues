﻿using ProjectShared.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.DAL
{
    public interface IUnitOfWork
    {
        IRepository<ProjectDTO> Projects { get; }
        IRepository<TaskDTO> Tasks { get; }
        IRepository<TeamDTO> Teams { get; }
        IRepository<UserDTO> Users { get; }
        void SaveChanges();
    }
}
