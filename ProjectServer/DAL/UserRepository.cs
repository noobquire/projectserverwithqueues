﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectShared.DTO;

namespace ProjectServer.DAL
{
    public class UserRepository : IRepository<UserDTO>
    {
        private readonly List<UserDTO> _users = new List<UserDTO>();

        public void Create(UserDTO item)
        {
            if (_users.Any(u => u.Id == item.Id)) throw new ArgumentException("Item already exists");
            _users.Add(item);
        }

        public void Create(IEnumerable<UserDTO> items)
        {
            foreach (var item in items) Create(item);
        }

        public void Delete(int id)
        {
            _users.RemoveAt(_users.FindIndex(u => u.Id == id));
        }

        public UserDTO Get(int id)
        {
            return _users.Find(u => u.Id == id);
        }

        public IEnumerable<UserDTO> GetAll()
        {
            return _users;
        }

        public void Update(UserDTO item)
        {
            _users[_users.FindIndex(u => u.Id == item.Id)] = item;
        }
    }
}