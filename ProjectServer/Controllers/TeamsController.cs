﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectShared.DTO;
using ProjectServer.DAL;
using ProjectServer.Services;
using ProjectServer.Interfaces;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITeamsService _teamsService;
        private readonly IQueueService _queueService;

        public TeamsController(IUnitOfWork unitOfWork, ITeamsService teamsService, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _teamsService = teamsService;
            _queueService = queueService;
        }

        // GET: api/Tasks
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            _queueService.PostValue("Getting all teams was triggered");
            return Ok(_unitOfWork.Teams.GetAll());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            _queueService.PostValue("Getting a team by id was triggered");
            return Ok(_unitOfWork.Teams.Get(id));
        }

        // POST: api/Tasks
        [HttpPost]
        public ActionResult Post([FromBody] TeamDTO value)
        {
            _queueService.PostValue("Creating a team was triggered");
            _unitOfWork.Teams.Create(value);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public ActionResult Post([FromBody] IEnumerable<TeamDTO> values)
        {
            _queueService.PostValue("Creating many teams was triggered");
            _unitOfWork.Teams.Create(values);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // PUT: api/Tasks
        [HttpPut]
        public ActionResult Put([FromBody] TeamDTO value)
        {
            _queueService.PostValue("Updating a team was triggered");
            _unitOfWork.Teams.Update(value);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _queueService.PostValue("Deleting a team was triggered");
            _unitOfWork.Teams.Delete(id);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("WithUsersOlderThan12")]
        public ActionResult<IEnumerable<UsersInTeamDTO>> GetTeamsWithUsersOlderThan12()
        {
            _queueService.PostValue("Getting teams with users older than 12 was triggered");
            return Ok(_teamsService.GetTeamsWithUsersOlderThan12());
        }
    }
}