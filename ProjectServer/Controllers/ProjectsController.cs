﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectServer.DAL;
using ProjectServer.Interfaces;
using System;
using ProjectShared.DTO;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProjectsService _projectsService;
        private readonly IQueueService _queueService;
        private readonly IMessageService _messageService;
        public ProjectsController(IUnitOfWork unitOfWork, IProjectsService projectsService, IQueueService queueService, IMessageService messageService)
        {
            _unitOfWork = unitOfWork;
            _projectsService = projectsService;
            _queueService = queueService;
            _messageService = messageService;
        }

        // GET: api/Projects
        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            _queueService.PostValue("Getting all projects was triggered");
            return Ok(_unitOfWork.Projects.GetAll());
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            _queueService.PostValue("Getting a project by id was triggered");
            return Ok(_unitOfWork.Projects.Get(id));
        }

        // POST: api/Projects
        [HttpPost]
        public ActionResult Post([FromBody] ProjectDTO value)
        {
            _queueService.PostValue("Creating a project was triggered");
            try
            {
                _unitOfWork.Projects.Create(value);
                _unitOfWork.SaveChanges();
                return Ok();
            } catch (ArgumentException)
            {
                return StatusCode(500);
            }
        }

        [HttpPost]
        [Route("CreateMany")]
        public ActionResult Post([FromBody] IEnumerable<ProjectDTO> values)
        {
            _queueService.PostValue("Creating many projects was triggered");
            _unitOfWork.Projects.Create(values);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // PUT: api/Projects/5
        [HttpPut]
        public ActionResult Put([FromBody] ProjectDTO value)
        {
            _queueService.PostValue("Updating a project was triggered");
            _unitOfWork.Projects.Update(value);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _queueService.PostValue("Deleting a project was triggered");
            _unitOfWork.Projects.Delete(id);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // GET: api/Projects/5/Stats
        [HttpGet]
        [Route("{id}/Stats")]
        public ActionResult<ProjectStatsDTO> GetStats(int id)
        {
            _queueService.PostValue("Getting project stats was triggered");
            return Ok(_projectsService.GetProjectStats(id));
        }
    }
}