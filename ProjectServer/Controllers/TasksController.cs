﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectShared.DTO;
using ProjectServer.DAL;
using ProjectServer.Interfaces;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;

        public TasksController(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        // GET: api/Tasks
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            _queueService.PostValue("Getting all tasks was triggered");
            return Ok(_unitOfWork.Tasks.GetAll());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            _queueService.PostValue("Getting a task by id was triggered");
            return Ok(_unitOfWork.Tasks.Get(id));
        }

        // POST: api/Tasks
        [HttpPost]
        public ActionResult Post([FromBody] TaskDTO value)
        {
            _queueService.PostValue("Creating a task was triggered");
            _unitOfWork.Tasks.Create(value);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public ActionResult Post([FromBody] IEnumerable<TaskDTO> values)
        {
            _queueService.PostValue("Creating many tasks was triggered");
            _unitOfWork.Tasks.Create(values);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // PUT: api/Tasks
        [HttpPut]
        public ActionResult Put([FromBody] TaskDTO value)
        {
            _queueService.PostValue("Updating a task was triggered");
            _unitOfWork.Tasks.Update(value);
            _unitOfWork.SaveChanges();
            return Ok();
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _queueService.PostValue("Deleting a task was triggered");
            _unitOfWork.Tasks.Delete(id);
            _unitOfWork.SaveChanges();
            return Ok();
        }
    }
}