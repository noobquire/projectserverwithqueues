using ProjectShared.DTO;
using System.Collections.Generic;

namespace ProjectServer.Interfaces
{
    public interface ITeamsService
    {
        IEnumerable<UsersInTeamDTO> GetTeamsWithUsersOlderThan12();
    }
}
