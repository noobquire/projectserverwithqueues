﻿using ProjectShared.DTO;

namespace ProjectServer.Interfaces
{
    public interface IProjectsService
    {
        ProjectStatsDTO GetProjectStats(int projectId); 
    }
}
