﻿using System;
using System.Collections.Generic;
using ProjectShared.DTO;

namespace ProjectManager
{
    public class User
    {
        public List<Task> Tasks;

        public User(UserDTO dto)
        {
            FirstName = dto.FirstName;
            LastName = dto.LastName;
            Email = dto.Email;
            Birthday = dto.Birthday;
            RegisteredAt = dto.RegisteredAt;
            if (dto.TeamId != null)
                Team = new Team(Api.GetTeamAsync(dto.TeamId.Value).Result);
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public Team Team { get; set; }
    }
}