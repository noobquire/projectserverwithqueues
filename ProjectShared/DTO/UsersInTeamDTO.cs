﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectShared.DTO
{
    [Serializable]
    public class UsersInTeamDTO
    {
        /// <summary>
        /// Null for users who do not participate in any teams
        /// </summary>
        [JsonProperty(PropertyName = "team_id")]
        public int? TeamId { get; set; }
        [JsonProperty(PropertyName = "team_name")]
        public string TeamName { get; set; }
        [JsonProperty(PropertyName = "users_in_team_ids")]
        public IEnumerable<int> UsersInTeamIds { get; set; }
    }
}
