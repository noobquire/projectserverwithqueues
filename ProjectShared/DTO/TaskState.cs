﻿using System;

namespace ProjectShared.DTO
{
    [Serializable]
    public enum TaskState
    {
        Created = 0,
        Started,
        Finished,
        Cancelled
    }
}